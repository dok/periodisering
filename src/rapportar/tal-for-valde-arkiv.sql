-- SQL*Plus-skript for � vise diverse tal for saker og journalpostar i valde arkiv (og nokre tal for eit personalarkiv)
-- Fila har ISO-8859-1-teiknkoding, jf. avsnittet �Om teknkoding� i README.md.
-- Sj� README.md for dokumentasjon.
--
-- Spesielt sp�rjingane for personalarkivet kan vere s�reigen for Noark-basen dei vart utvikla for. Men mykje anna kan gjenbrukast utan endringar med andre ephorte/Elements-system.

set echo off
set verify off
set feedback off
set define on
set pagesize 50000
set linesize 1000
-- Ikkje noko mellomrom f�r og etter sp�rjingar
set newpage none
clear columns
--column status format a6
column sakstype format a8
--column doktype format a7
column nemning format a70
--column jeining format a2
column dummy heading '' format a3
--column dummy noprint

clear computes
-- Eigen rad med sum blir ikkje fin med Pandoc
compute sum label 'sum' of mengd on dummy
break on dummy
set numformat ""
-- Talformatet blir heller ikkje fint :(
--alter session set nls_numeric_characters = ", ";
--set numformat 999G999
--set numformat 999,999,999

column sysdate new_value s_dato
select sysdate from dual;

-- Valde arkiv
def s_ar = &1
-- Vald arkivdel (for sp�rjingane for personalarkivet)
def s_ad = 'PERSONAL';

col s_ar_som_tekst new_value s_ar_som_tekst
select regexp_replace(q'{&s_ar}', '\(|\)|''', '') s_ar_som_tekst from dual;

-- Bruk filnamn med dato og klokkeslett for utdata
column dato_filnavn new_value s_dato_filnavn
select to_char(sysdate, 'yyyy-mm-dd"T"HH24MI') as dato_filnavn from dual;
def s_filnavn = 'tal-for-valde-arkiv-&s_dato_filnavn..md'
spool &s_filnavn

prompt ---
title: "Nokre relevante tal for arkiv som skal avleverast"
prompt subtitle: "Valde arkiv: &s_ar_som_tekst"
prompt date: &s_dato
prompt lang: nn-NO
prompt linkcolor: blue
prompt ...

prompt
prompt # Arkivdelar
prompt
prompt ## Ikkje utg�tte arkivdelar i arkiva
prompt
--prompt ~~~
select
    ad_arkdel as arkivdel
    ,ad_arkiv_ar as arkiv
    ,ad_primnok_op as primnyk
    ,ad_seknok_op as seknyk
    ,ad_fradato as fraa
    ,ad_tildato as til
from arkivdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and ad_astatus_as != 'U'
order by arkiv, arkivdel
;
--prompt ~~~


prompt
prompt
prompt # Saker
prompt
prompt ## Talet p� saker fordelt p� status
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ## Talet p� ikkje utg�tte saker fordelt p� arkivdel
prompt
--prompt ~~~
select null as dummy, sa_arkdel_ad as arkivdel, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
    and sa_status_ss != 'U'
group by sa_arkdel_ad
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ## Talet p� ikkje utg�tte saker fordelt p� tilgangskode
prompt
--prompt ~~~
select
    null as dummy
    ,count(sa_id) as mengd
    ,sa_tgkode_tk as tgkode
    ,tk_betegn as nemning
from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
inner join tgkode on sa_tgkode_tk = tk_tgkode
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
    and sa_status_ss != 'U'
group by sa_tgkode_tk, tk_betegn
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ## Talet p� ikkje utg�tte saker fordelt p� administrativ eining
prompt
--prompt ~~~
select
    null as dummy
    ,count(sa_id) as mengd
    ,ai_admkort || ' ' || ai_forkdn as adm_eining
from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join admindel on sa_admid_ai = ai_id
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_status_ss != 'U'
group by ai_admkort || ' ' || ai_forkdn
order by mengd desc
;
--prompt ~~~

column nemning format a37

prompt
prompt ## Talet p� ikkje utg�tte saker fordelt p� journaleining
prompt
--prompt ~~~
select
    null as dummy
    ,count(sa_id) as mengd
    ,sa_jenhet_je jeining
    ,je_betegn as nemning
from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join journenhet on sa_jenhet_je = je_jenhet
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
    and sa_status_ss != 'U'
group by sa_jenhet_je, je_betegn
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ## Klassering
prompt
prompt ### Talet p� ikkje utg�tte saker som manglar klassering
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
    and sa_status_ss != 'U'
    and sa_id not in (
        select kl_said_sa from klassering
)
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ## Talet p� saker (ikkje i status U eller R) som har feil prim�rklassering
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
left join klassering on sa_id = kl_said_sa
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and (kl_sort is null or kl_sort = 1)
    and (
        kl_ordnpri is null -- dvs. saken manglar klassering
        or ad_primnok_op != kl_ordnpri
    )
    and ad_arkiv_ar in &s_ar
    and sa_status_ss not in ('R', 'U')
    and sa_seknr != 0
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~


spool off
column saker_med_sakstype new_value s_sa_med_sakstype
select count(*) saker_med_sakstype from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and sa_seknr != 0
    and sa_status_ss != 'U'
    and sa_type_st is not null
;
spool &s_filnavn append
prompt
prompt ## Talet p� saker med ei sakstype
prompt
prompt Talet p� saker med ei sakstype (`SA_TYPE_ST`): &s_sa_med_sakstype
prompt
prompt Dersom talet er h�gare enn 0, b�r det unders�kjast n�rare.
undef s_sa_med_sakstype


prompt
prompt
prompt ## Arkivdelen &s_ad
prompt
prompt ### Talet p� ikkje utg�tte saker i arkivdelen &s_ad som manglar prim�rklassering
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkdel = '&s_ad'
    and sa_status_ss != 'U'
    and sa_seknr != 0
    and sa_id not in (
        select kl_said_sa from klassering where kl_sort = 1 and kl_ordnpri = 'ANR'
        )
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~

prompt
prompt ### Talet p� ikkje utg�tte saker i arkivdelen &s_ad som ikkje har ARKNOK som sekund�rklassering
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkdel = '&s_ad'
    and sa_status_ss != 'U'
    and sa_seknr != 0
    and sa_id in (
        select kl_said_sa from klassering where kl_sort = 1 and kl_ordnpri = 'ANR'
        )
    and sa_id not in (
        select kl_said_sa from klassering where kl_sort = 2 and kl_ordnpri = 'ARKNOK'
    )
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~


prompt
prompt ### Talet p� ikkje utg�tte saker utenfor arkivdelen &s_ad som har heimelen �Personalmappe�
prompt
--prompt ~~~
select null as dummy, sa_status_ss as status, count(sa_id) as mengd from noarksak
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkdel != '&s_ad'
    and sa_status_ss != 'U'
    and sa_seknr != 0
    and sa_uoff = 'Personalmappe'
group by sa_status_ss
order by count(sa_id) desc
;
--prompt ~~~

prompt
prompt
prompt # Journalpostar

prompt
prompt ## Talet p� journalpostar fordelt p� status
prompt
--prompt ~~~
select null as dummy, jp_status_js as status, count(jp_id) as mengd from journpost
inner join noarksak on sa_id = jp_said
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
group by jp_status_js
order by count(jp_id) desc
;
--prompt ~~~

prompt
prompt ## Talet p� ikkje utg�tte journalpostar fordelt p� dokumenttype
prompt
--prompt ~~~
select
    null as dummy
    ,count(jp_id) as mengd
    ,jp_ndoktype_nd as doktype
    ,nd_betegn as nemning
from journpost
inner join noarksak on sa_id = jp_said
inner join arkivdel on sa_arkdel_ad = ad_arkdel
inner join noarkdoktype on jp_ndoktype_nd = nd_doktype
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by jp_ndoktype_nd, nd_betegn
order by count(jp_id) desc
;
--prompt ~~~

column nemning format a70

prompt
prompt
prompt # Tilgangskodar
prompt
prompt ## Talet p� ikkje utg�tte journalpostar fordelt p� tilgangskode
prompt
--prompt ~~~
select
    null as dummy
    ,count(jp_id) as mengd
    ,jp_tgkode_tk as tgkode
    ,tk_betegn as nemning
from journpost
inner join noarksak on sa_id = jp_said
inner join arkivdel on sa_arkdel_ad = ad_arkdel
inner join tgkode on jp_tgkode_tk = tk_tgkode
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by jp_tgkode_tk, tk_betegn
order by count(jp_id) desc
;
--prompt ~~~

/*
prompt
prompt ## Talet p� dokument med tilgangskode som avvik fr� journalposten si, fordelt p� kombinasjon
prompt
--prompt ~~~
select null as dummy, count(db_dokid) as mengd, skjerming from (
    select
        db_dokid
        ,'dok. ' ||
            case
                when db_tgkode_tk is null then 'uskjerma'
                else db_tgkode_tk
            end || ' i JP ' ||
            case
                when jp_tgkode_tk is null then 'uskjerma'
                else jp_tgkode_tk
            end
        as skjerming
    from dokbeskriv
    inner join doklink on dl_dokid_db = db_dokid
    inner join journpost on dl_jpid_jp = jp_id
    inner join noarksak on jp_said = sa_id
    inner join arkivdel on sa_arkdel_ad = ad_arkdel
    where 1=1
        and ad_arkiv_ar in &s_ar
        and jp_status_js in ('J', 'A')
        and (
            db_tgkode_tk != jp_tgkode_tk
            or db_tgkode_tk is not null and jp_tgkode_tk is null
            or jp_tgkode_tk is not null and db_tgkode_tk is null
            )
)
group by skjerming
order by mengd desc
;
--prompt ~~~
*/


column nemning format a40


prompt
prompt ## Talet p� ikkje utg�tte journalpostar fordelt p� administrativ eining
prompt
--prompt ~~~
select
    null as dummy
    ,count(jp_id) as mengd
    ,ai_admkort || ' ' || ai_forkdn as adm_eining
from journpost
inner join noarksak on jp_said = sa_id
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join admindel on jp_admid_g = ai_id
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by ai_admkort || ' ' || ai_forkdn
order by mengd desc
;
--prompt ~~~


prompt
prompt ## Talet p� ikkje utg�tte journalpostar fordelt p� journaleining
prompt
--prompt ~~~
select
    null as dummy
    ,count(jp_id) as mengd
    ,jp_jenhet_g jeining
    ,je_betegn as nemning from journpost
inner join noarksak on sa_id = jp_said
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join journenhet on jp_jenhet_g = je_jenhet
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by jp_jenhet_g, je_betegn
order by count(jp_id) desc
;
--prompt ~~~


prompt
prompt
prompt # Dokument
prompt
prompt ## Talet p� dokument fordelt p� status i ikkje utg�tte journalpostar
prompt
--prompt ~~~
select
    null as dummy
    ,count(unique db_dokid) as mengd
    ,db_status_ds as status
from dokbeskriv
inner join doklink on dl_dokid_db = db_dokid
inner join journpost on dl_jpid_jp = jp_id
inner join noarksak on jp_said = sa_id
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by db_status_ds
order by mengd desc
;
--prompt ~~~


prompt
prompt ## Talet p� dokument fordelt p� variant i journalpostar med status J/A
prompt
--prompt ~~~
select
    null as dummy
    ,count(ve_dokid_db) as mengd
    ,ve_variant_vf as variant
    ,vf_betegn as nemning
from dokversjon
inner join dokbeskriv on ve_dokid_db = db_dokid
inner join doklink on dl_dokid_db = db_dokid
inner join journpost on dl_jpid_jp = jp_id
inner join noarksak on jp_said = sa_id
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join variantformat on ve_variant_vf = vf_kode
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js in ('J', 'A')
group by ve_variant_vf, vf_betegn
order by mengd desc
;
--prompt ~~~


prompt
prompt ## Talet p� dokument fordelt p� filformat (utanom arkivformat)
prompt
--prompt ~~~
select
    null as dummy
    ,count(ve_dokid_db) as mengd
    ,ve_dokformat_lf as filformat
    ,ve_variant_vf as variant
    ,case
        when ve_variant_vf = 'P' then null
        else vf_betegn
    end as nemning
from dokversjon
inner join dokbeskriv on ve_dokid_db = db_dokid
inner join doklink on dl_dokid_db = db_dokid
inner join journpost on dl_jpid_jp = jp_id
inner join noarksak on jp_said = sa_id
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join variantformat on ve_variant_vf = vf_kode
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js in ('J', 'A')
    and ve_variant_vf != 'A'
group by ve_variant_vf, vf_betegn, ve_dokformat_lf
order by mengd desc
;
--prompt ~~~


prompt
prompt ## Talet p� dokument per dokumentkategori i ikkje utg�tte journalpostar
prompt
--prompt ~~~
select
    null as dummy
    ,count(unique db_dokid) as mengd
    ,db_kategori_dk as kat
    ,dk_betegn as nemning
from dokbeskriv
inner join doklink on dl_dokid_db = db_dokid
inner join journpost on dl_jpid_jp = jp_id
inner join noarksak on jp_said = sa_id
inner join arkivdel on sa_arkdel_ad = ad_arkdel
left join dokkategori on db_kategori_dk = dk_kode
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
group by db_kategori_dk, dk_betegn
order by mengd desc
;
--prompt ~~~


prompt
prompt
prompt # Tal for heile Noark-basen
prompt
spool off
-- Vel journalpostar utan tilknytning til ei sak
column jp_utan_sak new_value s_jp_utan_sak
select count(jp_id) jp_utan_sak from journpost
where 1=1
    and jp_status_js != 'U'
    and not exists (
        select * from noarksak where jp_said = sa_id
    )
;
-- Vel saker utan tilknytning til ein arkivdel
column sa_utan_arkdel new_value s_sa_utan_arkdel
select count(*) sa_utan_arkdel from noarksak
where 1=1
    and sa_status_ss != 'U'
    and sa_arkdel_ad not in (
        select ad_arkdel from arkivdel
    )
;

spool &s_filnavn append
prompt
prompt ## Tal p� journalpostar utan sak og saker utan ein arkivdel
prompt
prompt Talet p� journalpostar utan tilknytning til ei sak: &s_jp_utan_sak
prompt
prompt Talet p� saker utan tilknytning til ein arkivdel: &s_sa_utan_arkdel
prompt
prompt Dersom eit tal er h�gare enn 0, b�r det unders�kjast n�rare.
undef s_jp_utan_sak
undef s_sa_utan_arkdel


prompt
prompt ## Talet p� dokumentversjonar som ikkje til knytt til tabellane `DOKBESKRIV`, `DOKLINK` eller `JOURNPOST`
prompt
--prompt ~~~
select null as dummy, count(ve_dokid_db) as mengd, mangel from (
    select
        null as dummy
        ,ve_dokid_db
        ,case
            when ve_dokid_db is null then 'DOKBESKRIV'
            when dl_dokid_db is null then 'DOKLINK'
            when jp_id is null then 'JOURNPOST'
            else 'ikkje noko'
        end as mangel
    from dokversjon
    left join dokbeskriv on ve_dokid_db = db_dokid
    left join doklink on dl_dokid_db = db_dokid
    left join journpost on dl_jpid_jp = jp_id
)
group by mangel
order by mengd desc
;
--prompt ~~~



undef s_ad;
undef s_ar;

spool off
