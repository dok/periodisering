#!/bin/bash

# 1. Konverter teiknkoding frå ISO-8859-1 (latin1) til UTF-8
# 2. Fjern skillelinja over totalen i tabellane
# 3. Konverter tabellane til pipe tables med Pandoc (slik at me i neste seg kan formatere tal med mellomrom)
# 4. Lag hardt mellomrom («No-Break Space») før dei siste seks og tre sifra i tal.
#   "s/\([0-9]\)\([0-9]\{3\}\)\([^0-9]\)/ # finn eitt siffer før tre siffer som verken følgjer eit punktum eller er følgt av eit siffer
#   \1$(echo -ne '\u00a0')\2\3/" # sett inn eit mellomrom etter det fyrste sifferet
# 5. Konverter fila til HTML med Pandoc og lagre ho med same namn.

if [ $# -eq 0 ]; then
    echo "Du oppgje fila (frå SQLcl) som eg skal konvertere, sjå README.md."
    exit 1
fi

filnamn=$(echo "$1" | cut -f 1 -d '.')

iconv -f ISO-8859-1 -t UTF-8 $1 | \
sed -e '/^\*\{3\} /d' | \
pandoc --from markdown --standalone --to markdown-simple_tables-multiline_tables-grid_tables | \
sed -e "s/\([^.][0-9]\)\([0-9]\{3\}\)\([^0-9]\)/\1$(echo -ne '\u00a0')\2\3/" \
-e "s/\([^.][0-9]\)\([0-9]\{3\}\)\([^0-9]\)/\1$(echo -ne '\u00a0')\2\3/" | \
pandoc --from markdown --standalone --table-of-contents --number-sections --out "$filnamn".html
