#!/bin/bash

# For å sjå kommandoene som vert utført, køyr:
# bash -x pdftests.sh

# https://stackoverflow.com/questions/2853803/how-to-echo-shell-commands-as-they-are-executed
# https://stackoverflow.com/questions/15461737/how-to-execute-xpath-one-liners-from-shell

for f in "$@"
do
    file "$f"
    echo
    pdfinfo "$f"
    echo
    pdfsig "$f"
    echo
    pdfdetach -list "$f"
    echo
    # Velg bare resultat av validering
    #../verapdf/verapdf "$f" | xmllint --xpath 'concat(//validationReport/@profileName," compliant: ",//validationReport/@isCompliant)' --format -
    # Velg resultat av validering og feil
    ../verapdf/verapdf "$f" | xmllint --xpath '//validationReport/@profileName | //validationReport/@statement | //rule/@clause | //rule/@status | //description/text()' --format -
    #if [[ "$f" == *arkiv* ]]; then
    #    ../verapdf/verapdf "$f"
    #else
    # Velg bare resultat av validering
    #    ../verapdf/verapdf "$f" | xmllint --xpath 'concat(//validationReport/@profileName," compliant: ",//validationReport/@isCompliant)' --format -
    # Velg resultat av validering og feil
    #../verapdf/verapdf "$f" | xmllint --xpath '//validationReport/@profileName | //validationReport/@statement | //rule/@clause | //rule/@status | //description/text()' --format -
    #fi
    echo
    echo
done
