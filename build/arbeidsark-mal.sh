#!/bin/bash

source $1
cat << EOF
-- SQL*Plus-skript: $tittel
-- Sjå README.md

-- Ikke vis «... rows selected.»
set feedback off
set verify off
set sqlformat html
spool off

-- Vel arkiv
def s_ar = "$2"

-- Formater liste over arkiv
col s_ar_som_tekst new_value s_ar_som_tekst
select regexp_replace(q'{&s_ar}', '\(|\)|''', '') s_ar_som_tekst from dual;

-- Lagre dagens dato
col dato new_value s_dato
select to_char(sysdate, 'YYYY-MM-DD') as dato from dual;

-- Sett filnamn og tittel
def s_filnamn = '$3'
def s_tittel = '$tittel (dato: &s_dato)'


-- 1. Lag ei fil med detaljar om kvart objekt
---------------------------------------------

spool &1./&s_filnamn.-&s_dato.-detaljar.html

prompt <h1>&s_tittel</h1>

$kommentar

-- $tittel
$detaljar
;


-- 2. Lag ei fil med ID-ane til fleire objekt samla
---------------------------------------------------
spool &1./&s_filnamn.-&s_dato.-DOK_ID-ar.html

prompt <h1>&s_tittel</h1>
EOF

if [ "$IDar_type" = "DOK_ID" ]; then
cat << EOF
prompt Du kan vise alle dokumenta som søkjeresultat i ephorte ved å gjere slik:<br />
-- Å bruke <ol> og <li> fungerer ikkje.
prompt 1. Kopier alt innhaldet eit felt i kolonna «DOK_ID» i tabellen (tal separert med komma).<br />
prompt 2. Gå til standard eller avansert søk i ephorte.<br />
prompt 3. Vel «Dokumenter» i rullegardinmenyen «Søk etter».<br />
prompt 4. Vel «Flere valg».<br />
prompt 5. I ein av rullegardinmenyane for DokBeskriv, vel «Dokument ID [DB_DOKID]». (Det vil seie i ein rullegardinmenyen til høgre for der det står «NoarkSak».)<br />
prompt 6. Lim inn verdiane du kopierte i steg 1, i det gule feltet til høgre for det som det står «Dokument ID [DB_DOKID]» i.<br />
prompt 7. Vel «Søk nå».<br />
prompt <br />
EOF
elif [ "$IDar_type" = "SA_ID" ]; then
cat << EOF
prompt Du kan vise alle sakene som søkjeresultat i ephorte ved å gjere slik:<br />
-- Å bruke <ol> og <li> fungerer ikkje.
prompt 1. Kopier alt innhaldet eit felt i kolonna «SA_ID» i tabellen (tal separert med komma).<br />
prompt 2. Gå til standard eller avansert søk i ephorte.<br />
prompt 3. Vel «Saksmapper/mapper» i rullegardinmenyen «Søk etter».<br />
prompt 4. Vel «Flere valg».<br />
prompt 5. I ein av rullegardinmenyane for NoarkSak, vel «Sak ID [SA_ID]». (Det vil seie i ein rullegardinmenyen til høgre for der det står «NoarkSak».)<br />
prompt 6. Lim inn verdiane du kopierte i steg 1, i det gule feltet til høgre for det som det står «Sak ID [SA_ID]» i.<br />
prompt 7. Vel «Søk nå».<br />
prompt <br />
EOF
fi

cat << EOF
$IDar1
$detaljar
$IDar2
;


spool off
set sqlformat default
undef s_ar
undef s_ar_som_tekst
undef s_filnamn
undef s_tittel
undef s_dato
EOF
