---
title: Om arbeidsarka
...

# Skript for å lage arbeidsark med objekt det må gjerast noko med

Skripta er testa med testa med SQLcl. Utdata har UTF-8-teiknkoding.

Kvart skript lagar to HTML-filer. Fila som sluttar på «detaljar», inneheld informasjon om kvart objekt (med eit objekt per rad). Fila som sluttar på «SA_ID-ar» og liknande, inneheld mange objekt i samme felt for å lett kunne vise alle saman som søkjeresultat i ephorte. (Sjå brukarrettleiing i fila.)

## Brukarrettleiing

1. Opprett mappa som arbeidsarka skal lagrast i. Dei må lagrast i ei anna mappa enn ho som SQL-skriptet ligg i.

    ```ps
    PS> mkdir c:\temp\Periodisering\utmappe
    ```

1. Logg på databasen med SQLcl.

<!--
```ps
PS C:\temp\Periodisering> ..\sqldeveloper-21.2.0.187.1842-x64\sqldeveloper\sqlcl\bin\sql uib@//dbora-eph-prod01.uio.no:1565/EPHPRD.uio.no
```
-->

1. Gå til mappa med skriptet du vil køyre:

    ```sql
    SQL> cd c:\temp\Periodisering
    ```
    (Du kan sjekke om SQLcl alt ser etter skript i mappa med kommandoen `show sqlpath`.)

    **NB:** Skriptet `alle.sql` vil berre fungere dersom du er i same mappe som skriptet; ellers er stiane til filene i det skriptet feil.

1. Køyr skriptet med mappa som arbeidsarka skal lagrast i, som parameter (utan skråstrek på slutten). Med `set termout off` vert resultatet ikkje vist i kommandolinja.

    ```sql
    SQL> set termout off
    SQL> @skript utmappe
    ```

Då vert to HTML-filer (med UTF-8-teiknkoding) lagra i mappa.


# Tips

* Dersom du ikkje liker formateringa av tabellane, kan du fjerne mykje av den med Pandoc ved å konvertere fila frå HTML til Markdown og tilbake: Køyr `pandoc -t markdown innfil.html | pandoc -o utfil.html`.

* Du kan åpne filene i Excel ved å endre filendingane frå html til xls.
