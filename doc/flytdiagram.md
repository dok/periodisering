# Avslutte sak vi antar er ukomplisert

```mermaid
graph TD
    A["Avslutte saksmappe vi antar er ukomplisert [v. 1.3.0]"] --> Aa[Åpne saken<br />ved behov.]
    Aa --> G
    erKlasseringenRiktig{Er klasseringen<br />riktig?}
    erKlasseringenRiktig -->|nei| korrigerKlasseringen[Korriger klasseringen.]
    erKlasseringenRiktig -->|ja| bevvurderSaksmappen
    bevvurderSaksmappen["Bevaringsvurder saksmappen<br />
    i henhold til det andre<br />
    flytdiagrammet. Fyll inn<br />
    resultatet i arket «ARKNOK<br />
    ja eller nei» i regnearket."]
    bevvurderSaksmappen --> skalSakenBevares
    bevvurderSaksmappen -->|"Nå ble det vanskelig!"| P
    P["La noen andre bryne seg<br />på sakstypen. Skriv «VK»<br />i regnearket for alle<br />saker av denne typen."]
    P --> Xa
    korrigerKlasseringen -->|"Nå ble det<br />vanskelig!"| X
    korrigerKlasseringen -->|OK!| erKlasseringenRiktig
    X["La noen andre<br />bryne seg på saken.<br />Skriv «V» i regnearket."]
    Xa[Gå til neste sak.]
    X --> Xa
    Xa --> A
    G{Er det godt mulig at<br />det kommer mer doku-<br />mentasjon i saken?}
    G -->|ja| Ga["Ikke endre saksstatus. Skriv<br />«B» i regnearket og en kom-<br />mentar i kolonnen «Begr.»<br />om hva du tror kommer."]
    G -->|nei| erKlasseringenRiktig
    Ga --> Xa
    skalSakenBevares{Skal saken<br />bevares?}
    skalSakenBevares -->|ja| N
    skalSakenBevares -->|nei| M
    skalSakenBevares -->|"Nå ble det<br />vanskelig!"| X
    M["Skriv «K» i regnearket.<br />Saken blir automatisk avsluttet.<br />"]
    M --> Xa
    N{Mangler saken<br />viktig doku-<br />mentasjon?}
    N -->|ja, tror det| O
    N -->|nei| K
    N -->|"Nå ble det vanskelig!"| P
    O["La noen andre bryne seg på det.<br />Skriv «M» i regnearket og en<br />kommentar i kolonnen «Begr.»<br />om hva du tror mangler."]
    O --> Xa

    K["Sørg for at at saksmappen og viktige journal-<br />poster i den er gjenfinnbare, det vil si at<br />metadata er korrekt og dekkende."]
    K --> I
    I["Avslutt saken.<br />Skriv «A» i regnearket."]
    I --> Xa
```

# Bevaringsvurdere dokumentasjon

```mermaid
graph TD
    A["Bevaringsvurdere dokumentasjon [v. 0.2.2]"]
    ARKNOK{"Hva står i arket<br />
    «ARKNOK ja eller nei»?<br />
    NB: At to saker har samme<br /
    ordningsverdi, betyr ikke<br />
    at BK-vurderingen er lik."}
    ARKNOK --> |Skal bevares.| B1
    ARKNOK --> |Omtales ikke.| FUP
    ARKNOK --> |Kan kasseres.| K
    A --> ARKNOK
    FUP{"Hva står<br />
    i Fuppen?"}
    oppdaterRegnearkBevares["Oppdater arket<br />«ARKNOK ja eller nei»."]
    FUP -->|Skal bevares.| oppdaterRegnearkBevares
    oppdaterRegnearkBevares --> B1
    FUP -->|Omtales ikke.| FUP2[Oioi! Gi]
    oppdaterRegnearkKasseres["Oppdater arket<br />«ARKNOK ja eller nei»."]
    FUP -->|Kan kasseres.| oppdaterRegnearkKasseres
    oppdaterRegnearkKasseres --> K
    K["Kan kasseres<br />
    (før eller siden)."]
    FUP2["Oioi! Gi be-<br />
    skjed til Per<br/>
    Christian"]
    FUP2 --> F1s
    F1["F1: «å dokumentere offentlige organers funksjoner i<br />
    samfunnet, deres utøvelse av myndighet, deres rolle<br />
    i forhold til det øvrige samfunn og deres rolle i<br />
    samfunnsutviklingen»<br />
    <b>Kriterier</b><br />
    - Administrativt nivå<br />
    - Saksbehandlingstype<br />
    - Saksbehandlingsledd<br />
    - Ekstraordinære/ordinære aktiviteter<br />
    - Pionervirksomhet<br />
    - Primær/internfunksjon i virksomheten
    "]
    F1s{"F1: Dokumenta-<br />
    sjonsverdi for<br />
    ettertiden?"}
    F1s -->|ja| U
    F1s -.- F1
    F1s -->|nei| F2s
    vanskelig["Hvis du jobber etter<br />diagrammet for<br />avslutning av saker:<br />La noen andre bryne seg<br />på sakstypen. Skriv «VK»<br />i regnearket for alle<br />saker av denne typen.<br />og ta for deg en annen."]
    F1s -->|Nå ble det vanskelig.| vanskelig
    F2["F2: «å holde tilgjengelig materiale som gir<br />
    informasjon om forhold i samfunnet på et gitt<br />
    tidspunkt, og som belyser samfunnsutviklingen»<br />
    <b>Kriterier</b><br />
    - Tidsspenn/kontinuitet<br />
    - Omfang<br />
    - Informasjonstetthet, tematisk variasjon<br />
    - Lenkbarhet med annet arkivmateriale<br />
    - Kvalitative egenskaper<br />
    - Alder
    "]
    F2s{"F2: Informa-<br />
    sjonsverdi for<br />
    ettertiden?<br />
    "}
    F2s -->|ja| U
    F2s -->|nei| F3s
    F2s -.- F2
    F3["F3: «å dokumentere personers og virksom-<br />
    heters rettigheter og plikter i forhold til<br />
    det offentlige, og i forhold til hverandre»<br />
    <b>Kriterier</b><br />
    - Saksbehandlingens konsekvenser<br />
    - Hjemmelsgrunnlag
    "]
    F3s{"F3: Dokumenterer<br />
    rettigheter<br />
    for andre?
    "}
    F3s -.- F3
    %%F3s -->|ja| U
    %%F3s -->|nei| F4
    %%F4["F4: «å dokumentere de arkivskapende organers
    %%rettigheter og plikter i forhold til andre
    %%instanser»
    %%<b>Kriterier</b>
    %%- Administrative og driftsmessige behov
    %%"]
    %%F4s{"F4: Informasjonsverdi
    %%for ettertiden?
    %%"}
    F3s -->|ja| U2
    F3s -->|nei| B3
    U{Unikt?}
    U -->|ja| B1[Skal bevares.]
    U -->|nei| B3
    B3["Bevares etter<br />
    arkivskapers be-<br />
    hov. Kan deretter<br />
    kasseres."]
    U2[Unikt?]
    B2["Bevares inntil doku-<br />
    mentasjonsbehovet<br />
    opphører"]
    B2 --> K
    B3 --> K
    U2 -->|ja| B2
    U2 -->|nei| B3
```
