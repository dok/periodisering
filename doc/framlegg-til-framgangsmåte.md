---
title: Framlegg til framgangsmåte for periodisering av arkiv
...

Dette er eit framlegg til framgangsmåte for å periodisere eit arkiv i ein Noark-base som òg inneheld andre arkiv. Nokre av oppgåvene er ikkje naudsynte dersom Noark-basen berre inneheld eitt arkiv.


# Føresetnadar

Sjå README.md for sakarkivsystem og database.

* Arkivet skal avleverast til Arkivverket.
* Vi avleverer alle variantar av dokument (ikkje berre arkivformat).
* Det finst ein BK-plan som Riksarkivaren har godkjent.
* Vi bryr oss mindre om saker som kan kasserast, enn dei som skal bevarast.
* Vi ser på kontrollerer ikkje systematisk skjerming, namngiving og dokument for journalpostar som er journalført.
* Vi ser på skjerming i journalførte journalpostar som kontrollert.

«teknisk»/«SQL» tyder at oppgåva må eller bør gjerast ved hjelp av skripting.

«prosjekt» tyder at prosjektmedarbeidarar etter planen skal gjere dette.

«drift» tyder at driftsmedarbeidere etter planen skal gjere dette.

«avklaring» tyder at spørsmålet må avklarast på eit overordna plan.

# Avklar overordna spørsmål [avklaring]

## Ressursar og tekniske føresetnadar

## Arkivavgrensing og kassasjon

1. Kartlegg arkivet og fyll ut [skjema for arkivbeskriving (av elektronisk arkiv)](https://www.arkivverket.no/for-arkiveiere/arkivavslutning-og-innlevering2/overf%C3%B8ring-av-elektroniske-arkiver).
1. Avklar kva reglar som gjeld for kassasjon.


# Ordne

Dersom arkivet fortsatt er i bruk når ein ordnar det, må disse oppgåvene òg gjerast på nytt når det ikkje lenger er i bruk.


## Rydd i kva for arkiv/arkivdelar som skal brukast [gjort 2021-32]

1. Identifiser arkiv/arkivdelar som ikkje skal brukast.
1. Flytt eventuelle saker og journalpostar frå arkiva/arkivdelane.
1. Sett arkiva/arkivdelane som utgått.


## Rydd i kva for administrative einingar som skal brukast [avklaring]

1. Sjå spørjing i rapporten for talet på saker og journalpostar ved kvar eining
1. Identifiser administrative einingar som ikkje skal brukast.
1. Flytt eventuelt saker og journalpostar frå einingane dersom dei ikkje skal vere der.
1. Avslutt eventuelt einingane.


## Rydd i kva for journaleiningar som skal brukast [drift]

1. Identifiser journaleiningar som ikkje skal brukast.

    I rapporten ser du kor mange saker i arkivet som ligg ved journaleiningar som tilhører andre arkiv.

1. Flytt eventuelle saker og journalpostar frå journaleiningane.

    I rapporten ser du kor mange saker i arkivet som ligg ved journaleiningar som tilhører andre arkiv. Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

    * tabell: NoarkSak; kolonne: Journalenhet; verdi: éi eller fleire journaleiningar (skilde med komma)
    * tabell: Arkivdel; kolonne: Arkiv [AD_ARKIV_AR]; verdi: eitt eller fleire arkiv (skilde med komma)

1. Avslutt journaleiningane.


## Rydd i kva for tilgangskodar som skal brukast [avklaring, drift]

1. Identifiser hva for tilgangskodar som ikkje skal brukast.

    I rapporten ser du kor mange saker og journalpostar i arkivet som kva for tilgangskoder.

1. Dokumenter kva slags tilgangskodar som (skal) brukast for kva.

1. Endre tilgangskode på aktuelle saker, journalpostar og dokument.

    I rapporten ser du kor mange saker og journalpostar i arkivet som kva for tilgangskoder.
    
    **Saker:** Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

    * tabell: NoarkSak; kolonne: Tilgangskode [SA_TGKODE_TK]; verdi: éi eller fleire tilgangskodar (skilde med komma)
    * tabell: Arkivdel; kolonne: Arkiv [AD_ARKIV_AR]; verdi: eitt eller fleire arkiv (skilde med komma)

    Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med:
    
    * tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

    **Journalpostar:** Bruk avansert søk med «flere valg» i ephorte og søk etter journalpostar det gjeld, med:

    * tabell: NoarkSak; kolonne: Tilgangskode [SA_TGKODE_TK]; verdi: éi eller fleire tilgangskodar (skilde med komma)
    * tabell: NoarkSak; kolonne: Arkivdel [SA_ARKDEL_AD]; verdi: alle arkivdelane i arkivet/arkiva (sjå tabell i rapporten)(skilde med komma)

    Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med:
    
    * tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

    **Dokument:** Må truleg gjerast med ei spørjing.

1. Avslutt tilgangskodane.

1. Identifiser saker i feil arkivdel.

    Det er ikkje alltid så lett. Men nokre kan ein finne med søka under avsnittet «[Klasser saker riktig]».

1. Flytt dei til riktig arkivdel (og endre klassering dersom naudsynt).


## Klasser saker riktig [SQL, drift/prosjekt]

I rapporten ser du kva for arkivdelar som har kva for primærnynkel (og eventuelt sekundærnykel)

Identifiser sakene og rett klasseringan(e).

### Saker utan naudsynt ordningsprinsipp (primært eller sekundært)

**Manglande (primær)klassering:** Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

* tabell: NoarkSak; kolonne: Arkivdel [SA_ARKDEL_AD]; verdi: namnet på éin arkivdel
* tabell: NoarkSak; kolonne: Status [SA_STATUS_SS]; verdi: «A,B,F,X» (utan hermeteikn)[^ikkje-R]
* tabell: Klassering; kolonne: Ordningsprinspp [KL_ORDNPRI]; verdi: «-» (utan hermeteikn) (ignorer feilmeldinga du får)

Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med

* tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

**Manglande sekundærklassering:** Kan berre gjerast med ei spørjing?


### Saker med feil ordningsprinsipp (primært eller sekundært)

**Feil primærklassering:** Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

* tabell: NoarkSak; kolonne: Arkivdel [SA_ARKDEL_AD]; verdi: namnet på éin arkivdel
* tabell: NoarkSak; kolonne: Status [SA_STATUS_SS]; verdi: «A,B,F,X» (utan hermeteikn)[^ikkje-R]
* tabell: Klassering; kolonne: Ordningsprinspp [KL_ORDNPRI]; verdi: «<>» (som tyder «ikkje») (utan hermeteikn) og arkivdelen sin primærnykel
* tabell: Klassering; kolonne: Sortering [KL_SORT]; verdi: 1

Til dømes: arkivdel PERSONAL, klassering «<>ANR», sortering 1, eller arkivdel SAKSARKIV, klassering «<>ARKNOK», sortering 2.

Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med

* tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

**Feil sekundærklassering:** Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

* tabell: NoarkSak; kolonne: Arkivdel [SA_ARKDEL_AD]; verdi: namnet på éin arkivdel
* tabell: NoarkSak; kolonne: Status [SA_STATUS_SS]; verdi: «A,B,F,X» (utan hermeteikn)[^ikkje-R]
* tabell: Klassering; kolonne: Ordningsprinspp [KL_ORDNPRI]; verdi: «<>» (som tyder «ikkje») (utan hermeteikn) og arkivdelen sin sekundærnykel
* tabell: Klassering; kolonne: Sortering [KL_SORT]; verdi: 2

Til dømes: arkivdel PERSONAL, klassering «<>ARKNOK», sortering 2

Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med:

* tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

[^ikkje-R]: Vi søker altså ikkje etter saker med status R, da vi føreset at dei blir kontrollert på anna vis.


#### Saker som har FNR eller FNS som ordningsprinsipp

Sjå arbeidsarket «Saker-med-FNR-eller-FNS-som-ordningsprinsipp». Det inneheld saker frå alle arkivdeler.

1. Sett saka i status B eller R.
1. Vel riktig(e) ordningsprinpp og verdiar. (Dersom saka hadde status\ U, kan du berre slette klasseringan(e).)
1. Sett saka tilbake i opphavleg status.

Dersom ordningsprinsippa skal slettast, er det kanskje naudsynt å fyrst slette ordningsverdiane manuelt.


### Saker med feil ordningsverdi

Det gjeld til dømes alle saker med ordningsverdi ANR som ikkje har seks- til åttesifra ordningsverdi.


#### ANR-ordningsverdier som òg finst som ARKNOK-ordningsverdi

Sjå arbeidsarket «Saker-med-ANR-ordnverdi-lik-ARKNOK-ordnverdi». Det inneheld saker frå alle arkivdeler.

1. Sett saka i status B eller R.
1. Vel riktig(e) ordningsprinpp og verdiar. Dersom saka hadde status\ U, kan du berre slette klasseringan(e)
1. Sett saka tilbake i opphavleg status.

Spørjinga inkluderer òg saker i status U. Desse kan ein utelukke dersom ein berre skal ordne eit avslutta arkiv. Men under arkivdanninga kan det å slette dei også føre til at dei ikkje blir forvalgt. I så fall er det kanskje naudsynt å slette ordningsverdiane manuelt.


## Sørg for at alle journalpostar har journaldato innanfor journalperioda

1. Identifiser journalpostar med journaldato før byrjinga på journalperioda (eller med journaldato etter slutten på ho)

    ```sql
    -- Vel journalpostar med journaldato utanfor journalperioda
    -- berre éin?
    select
        jp_id
        ,jp_seknr || '/' || jp_jaar as lnr
        ,jp_status_js as js
        ,jp_dokdato
        ,jp_opprettetdato_g
        ,jp_jdato
        ,ad_fradato
        ,ad_tildato
        ,ad_arkdel
        ,jp_innhold_g
    from journpost
    inner join noarksak on sa_id = jp_said
    left join arkivdel on sa_arkdel_ad = ad_arkdel
    where
        1=1
        and ad_arkiv_ar in &s_ar
        and jp_status_js != 'U'
        and (jp_jdato < ad_fradato or jp_jdato > ad_tildato)
    order by jp_jdato
    ;
    ```

1. Endre dato.


## Rydd i saker utan journalpostar og journalpostar utan dokument

1. Finn saker utan journalpostar og journalpostar utan dokument

    **Saker utan journalpostar:** Bruk avansert søk med «flere valg» i ephorte og søk etter saker det gjeld, med:

    * tabell: NoarkSak; kolonne: Ant.poster [SA_ANTJP]; verdi: 0
    * tabell: NoarkSak; kolonne: Status [SA_STATUS_SS]; verdi: «A,B,F,X» (utan hermeteikn)[^ikkje-R]
    * tabell: Arkivdel; kolonne: Arkiv [AD_ARKIV_AR]; verdi: eitt eller fleire arkiv (skilde med komma)

    Du kan avgrense søket til den administrative eininga som rolla di har som standard, ved å i tillegg søkje med:
    
    * tabell: NoarkSak; kolonne: Journalenhet; verdi: «|JE_JENHET|» (utan hermeteikn)

    > **NB:** Ikkje alle saker utan journalpostar har verdien 0 i kolonna Ant.poster. Ein må bruke ei spørjing for å finne resten.

    ```sql
    -- Vel saker uten journalpostar (som ikkje er utgått)
    select sa_id, sa_saar || '/' || sa_seknr as saksref, sa_status_ss as st, sa_dato, sa_tittel from noarksak
    left join arkivdel on sa_arkdel_ad = ad_arkdel
    where 1=1
        and ad_arkiv_ar in &s_ar
        and sa_status_ss != 'U'
        and sa_seknr != 0
        and not exists (
            select * from journpost where jp_said = sa_id and jp_status_js != 'U'
        )
    order by sa_id
    ;
    ```

    **Journalpostar utan dokument(filer):** Ein må bruke ei spørjing.

    ```sql
    -- Vel journalpostar (som ikkje er utgått) utan dokument
    -- 1188 per 02.09.2021
    select jp_id, jp_seknr || '/' || jp_jaar as lnr, jp_status_js as js, jp_opprettetdato_g, jp_innhold_g from journpost
    inner join noarksak on jp_said = sa_id
    left join arkivdel on sa_arkdel_ad = ad_arkdel
    left join doklink on dl_jpid_jp = jp_id
    left join dokbeskriv on dl_dokid_db = db_dokid
    where 1=1
        and ad_arkiv_ar in &s_ar
        and jp_status_js != 'U'
        and not exists (
            select * from dokversjon where ve_dokid_db = db_dokid
        )
    order by jp_id
    ;
    ```

1. Vurder om dei einskilde sakene og journalpostane skal settast som utgått eller om ein skal prøve å oppdrive dokumenta.


## Korriger (nokre) dokumentdatoar [SQL]

Det er ikkje realistisk at alle dokumentdatoane er rett, men ein kan i alle fall finne dei med dokumentdato langt utanfor arkivperioda og rette dei med eit skript.

1. Identifiser journalpostar med dokumentdato
2. Rett datoane.


## Leit etter saker og journalpostar som skal arkivavgrensast [SQL, drift/prosjekt]

```sql
-- Velg saker med et av ordene i tittelen: utgår, utgått, test, testsak, testmappe, slett, slettes, flytt, flyttet, flyttes
-- TODO: det er vel nok med slett, flytt, utgå osv.?
select * from noarksak
where 1=1
    and sa_seknr != 0
    and sa_status_ss != 'U'
    and regexp_like(lower(sa_tittel), '(^| )(utgår|utgått|test(|sak|mappe)|slett(es|)|flytt(et|es|))($| )', 'i')
    --and rownum <= 100
;
```


## Klasser utgåtte saker? [SQL]

Dette er kanskje naudsynt for å produsere eit Noark\ 5-uttrekk, siden `mappe` ligger under `klasse`.


## Rett mishøve mellom saksbehandlar i `journpost` og behandlingsansvarleg i `avsmot`

```sql
select jp_id, am_id, jp_innhold
    ,jp_sbhid_g
    ,(select pn_init from pernavn where pn_id = jp_sbhid_g) jp_sbh
    ,(select pn_init from pernavn where pn_id = am_sbhid_pn) am_sbh
from journpost
inner join avsmot on jp_id = am_jpid
where 1=1
    and jp_sbhid_g != am_sbhid_pn
    and am_behansv = -1
    and (jp_ndoktype_nd = 'I' and am_ihtype = -1 or jp_ndoktype_nd != 'I')
    --and jp_sbhid_g != '19010' -- SAP
;
```

# Registrer fysisk arkiv

Det kan finnast fysisk arkiv frå same periode. Sjekk om det er arkivert i ephorte. Dersom ikkje skann det og arkiver det eller ordne det i stykke og opprett saker/journalpostar som viser til stykka


# Kontroller

## Kontroller at klasseringar med saker som kan kasserast, berre inneheld slike saker [SQL, drift/prosjekt]

1. Identifiser klasseringane som vert brukt av flest journalpostar (gjennom tilknytning til saker), og som ein vil kassere.
1. Sjekk at sakene det gjeld, er riktig klassert. Endre ho eventuelt.


## Sjekk mappedokument (dokumenttype Y og Z) [teknisk, SQL, prosjekt]

Desse kan ikkje kasserast utan gyldig heimel, jf. arkivlova §\ 9.

Per i dag vert dokumenttype Z ikkje vist i ephorte.

Dokumenttypa bør endrast med eit skript.

## Finn gamle journalpostar i status R eller M [drift]

## Ta hand om dokumentpostar

### Skaff eit oversyn over «dokumentpostar i mappe» (dokument med dokumenttype Z)

### Konverter dokumentpostar i mappe til dokumentpost i saksmappe (Y-notat)

NB: Inkluderer ikkje objekta frå noverande år fordi sekvensar er i bruk.

```sql
def s_init = 'PEGA'

/* Loggfør endringar av `noarksak` */

merge into logginfo_g l
using (
    select
        sa_id
        ,sa_saar
        ,sa_mappetype_g
        ,sa_tgkode_tk
        ,sa_tggruppe_tg
        ,sa_admid_ai
        ,sa_ansvid_pn
        ,pn_id
        ,pn_navn
        ,to_char(sysdate, 'YYYY') noverande_aar
    from noarksak, pernavn
    where 1=1
        and sa_status_ss != 'U'
        and sa_seknr = 0
        and sa_mappetype_g in ('AM', 'FM', 'PM') -- adm. einigs-, felles-, privat mappe
        and pn_init = '&s_init'
        and sa_saar != to_char(sysdate, 'YYYY')
        and sa_id in ( select jp_said from journpost where jp_status_js != 'U' )
) sa_kj
on (1=0)
when not matched then
    insert (
        lig_id
        ,lig_tabellnavn_lfg
        ,lig_feltnavn_lfg
        ,lig_noekkel0
        ,lig_noekkelfelt
        ,lig_hendelse_lih
        ,lig_tekst
        ,lig_regkl
        ,lig_regdato
        ,lig_regav_pn
        ,lig_pvgav_pn
        ,lig_noark
        ,lig_tgkode_tk
        ,lig_tggruppe_tg
        ,lig_admid_ai
        ,lig_sbhid_g
        ,lig_said_sa
        ,lig_opprettetdato_g
        ,lig_sistoppdatert_g
        ,lig_sistoppdatertav_pn
    ) values (
        eph_seq_lig_id.nextval
        ,'NOARKSAK'
        ,'SA_MAPPETYPE_G'
        ,sa_kj.sa_id
        ,'SA_ID'
        ,'2'
        ,'old:('|| sa_kj.sa_mappetype_g || '),new:(),field:(Mappetype),pnid:(' || sa_kj.pn_id || '),name:(' || sa_kj.pn_navn || ')'
        ,sysdate
        ,sysdate
        ,sa_kj.pn_id
        ,'0' -- LIG_PVGAV_PN
        ,'-1'
        ,sa_kj.sa_tgkode_tk
        ,sa_kj.sa_tggruppe_tg
        ,sa_kj.sa_admid_ai
        ,sa_kj.sa_ansvid_pn
        ,sa_kj.sa_id
        ,sysdate
        ,sysdate
        ,sa_kj.pn_id
    )
;
```

For saker frå inneverande år må ein bruke sekvensen i staden.


```sql
-- Inkluderer ikkje aarets saker
merge into noarksak sa_m
using (
    select
        sa_id
        ,sa_saar
        ,sa_mappetype_g
        ,to_char(sysdate, 'YYYY') noverande_aar
        ,pn_id
        ,pn_navn
    from noarksak, pernavn
    where 1=1
        and sa_status_ss != 'U'
        and sa_seknr = 0
        and sa_mappetype_g in ('AM', 'FM', 'PM') -- adm. einigs-, felles-, privat mappe
        and pn_init = '&s_init'
        and sa_saar != to_char(sysdate, 'YYYY')
        and sa_id in ( select jp_said from journpost where jp_status_js != 'U' )
) sa_kj
on (sa_kj.sa_id = sa_m.sa_id)
when matched then
    update set
        sa_m.sa_seknr = 1 + (select max(sa_seknr) from noarksak where sa_saar = sa_kj.sa_saar)
            --SQL Error: ORA-02287: sekvensnummer er ikke tillatt her
            --when sa_kj.sa_saar = sa_kj.noverande_aar then (select eph_seq_sa_seknr.nextval from dual)
        ,sa_m.sa_mappetype_g = ''
        ,sa_m.sa_div0_g = sa_kj.sa_mappetype_g
        ,sa_sistoppdatert_g = sysdate
        ,sa_sistoppdatertav_pn = pn_id
;

/*
TODO: Hvis uttrykket vert brukt på saker frå inneverande år, oppdater deretter
`last_number` for sekvensen
*/
```

`sa_m.sa_sekr` fekk same verdi for alle mappene innanfor eit år. Derfor var det naudsynt med følgjande spørring:

(Men ideelt sett skulle `row_number()` ha blitt nullstilt for kvart år for å unngå gap i nummereringa.)

```sql
merge into noarksak m
using (
    select
        row_number() over(order by sa_id) + seknr_max - 1 as nytt_seknr
        ,sa_id
        ,sa_saar
        ,sa_seknr
        ,seknr_max
    from (
        select
            sa_id
            ,sa_saar
            ,sa_seknr
            ,(select max(sa_seknr) from noarksak a where a.sa_saar = b.sa_saar) as seknr_max
        from noarksak b
        where 1=1
            and sa_div0_g in ('AM', 'FM', 'PM')
    )
) k
on (k.sa_id = m.sa_id)
    when matched then
        update set m.sa_seknr = k.nytt_seknr
;
```

```sql
merge into logginfo_g l
using (
    select
        jp_id
        ,jp_said
        ,jp_ndoktype_nd
        ,jp_tgkode_tk
        ,jp_tggruppe_tg
        ,jp_admid_g
        ,jp_sbhid_g
        ,pn_id
        ,pn_navn
    from journpost, pernavn
    where 1=1
        and jp_ndoktype_nd = 'Z'
        and jp_status_js != 'U'
        and pn_init = '&s_init'
        and jp_jaar != to_char(sysdate, 'YYYY')
) kj
on (1=0)
when not matched then
    insert (
        lig_id
        ,lig_tabellnavn_lfg
        ,lig_feltnavn_lfg
        ,lig_noekkel0
        ,lig_noekkelfelt
        ,lig_hendelse_lih
        ,lig_tekst
        ,lig_regkl
        ,lig_regdato
        ,lig_regav_pn
        ,lig_pvgav_pn
        ,lig_noark
        ,lig_tgkode_tk
        ,lig_tggruppe_tg
        ,lig_admid_ai
        ,lig_sbhid_g
        ,lig_jpid_jp
        ,lig_said_sa
        ,lig_opprettetdato_g
        ,lig_sistoppdatert_g
        ,lig_sistoppdatertav_pn
    ) values (
        eph_seq_lig_id.nextval
        ,'JOURNPOST'
        ,'JP_NDOKTYPE_G'
        ,kj.jp_id
        ,'JP_ID'
        ,'2'
        ,'old:('|| kj.jp_ndoktype_nd || '),new:(Y),field:(Dokumenttype),pnid:(' || kj.pn_id || '),name:(' || kj.pn_navn || ')'
        ,sysdate
        ,sysdate
        ,kj.pn_id
        ,'0' -- LIG_PVGAV_PN
        ,'-1'
        ,kj.jp_tgkode_tk
        ,kj.jp_tggruppe_tg
        ,kj.jp_admid_g
        ,kj.jp_sbhid_g
        ,kj.jp_id
        ,kj.jp_said
        ,sysdate
        ,sysdate
        ,kj.pn_id
    )
;


merge into journpost m
using (
    select
        jp_id
        ,jp_jaar
        ,jp_ndoktype_nd
        ,pn_id
    from journpost, pernavn
    where 1=1
        and jp_ndoktype_nd = 'Z'
        and jp_status_js != 'U'
        and pn_init = '&s_init'
        and jp_jaar != to_char(sysdate, 'YYYY')
) kj
on (m.jp_id = kj.jp_id)
when matched then
    update set
        /* sekv.nr. er 0 både for Z- og Y-notat */
        m.jp_ndoktype_nd = 'Y'
        ,m.jp_div0_g = 'Tidligere Z-notat'
        ,m.jp_sistoppdatert_g = sysdate
        ,m.jp_sistoppdatertav_pn = pn_id
;
```

### Skaff eit oversyn over «dokumentpostar i saksmappe» (dokument med dokumenttype Y)

Skal dei arkivavgrensast eller vere med i uttrekket?


### Arkivavgrens dokumentpostar i saksmapppe hvis dei skal vere med i uttrekket.


## Sjekk at sensitivt innhald er riktig skjerma [avklaring, drift/prosjekt]

Særleg det som gjeld sensitive student- eller personalsaker, inkludert varsel.


## Avslutt saker [Arkivverket, SQL, drift/prosjekt]

*Utkast til rutine:*

1. Sjekk at saka (og journalpostane) ikkje skal arkivavgrensast. Då kan du i staden sette ho/dei i status\ U.

1. Sjekk at saka ikkje har openberre manglar, det vil seie at

    * ho er ferdigbehandla
    * ho er ikkje (openberr) ufullstendig
    * ho har ein dekkjande tittel
    * klasseringa er riktig
    * skjerminga er riktig
    * eventuelle journalpostar som skal arkivavgrensast, har status\ U
    * journalpostane har ei dekkjande innhaldsbeskriving («journalposttittel»)
    * journalpostane har riktig avsendar/mottakar
    * journalpostane har riktig skjerming
    * dokumentdato ikkje er (openbert) feil
    * dokumenta er leseleg (bruk førehandsyninga i Elements)

1. Avslutt saka og la systemet avskrive alle restansar.


## Saksframlegg (i møte- og utvalsmodulen) [???]

Kva må ein gjere med dei?


# Dokument (vert gjort i denne rekkjefølgja)

## Rydd opp i dokument som manglar i dokumentlageret og/eller er utsjekka [SQL, prosjekt]

«Dokumentpostar i mappe» (dokumenttype Z) kan med fordel ignorerast

Bruk arbeidsarket «Dok-ikkje-i-doklager» for å finne dokumenta det gjeld.

* Skal dokumentet arkivavgrensast, kan du berre slette det.

* Dersom det fins fleire versjonar eller format av same dokument, kan du slette versjonen/formatet som er tom(t).

* Dersom dokumentet finst ein annan stad i journalposten eller saka, kan du slette dokumentoppføringa for det dokumentet som manglar.

* Elles må du vurdere for kvart dokument einskilt kor viktig det er for saka. Alt etter bør du prøve å få tak i det. (I arbeidsarket ser du om personen har ei aktiv rolle i ephorte.) Dersom det ikkje lykkast, skriv ein merknad til journalposten om at dokumentet ikke lot seg oppdrive.


## Rydd opp i utsjekka dokument [SQL, prosjekt]

Gjer dette etter at du har rydda opp i dokument som manglar i dokumentlageret.

Bruk arbeidsarket «Dok-utsjekka» for å finne dokumenta det gjeld.

Framgangsmåten er nesten lik som for dokument som manglar i dokumentlageret. Men dersom du sjekker inn dokumentet, må du òg vurdere om det ser ferdig ut. Viss det ikke gjer det og det i tillegg er viktig dokumentasjon, må du ta kontakt med saksbehandlaren for å høyre som dokumentet vart ferdigstilt ein annan stad, og eventuelt få tak i eit fullstendig dokument.

## Ta hand om dokument i status B («under behandling») [prosjekt, SQL]

Det vil i stor grad seie å ta hand om gamle journalpostar i status B eller M. Men ein bør òg finne eventuelle andre med ei spørjing.


## Undersøk dokument som manglar produksjonsformat [SQL]

I minst eitt tilfelle fanst eit dokument berre i arkivformat i `dokversjon` med `ve_variant_vf = 'A'` og `ve_filref = '2020\11\24\1880729.PDF'`. I dokumentlageret var fanst fila `2020\11\24\1880729.PDF`. Feilmeldinga i ephorte var «Specified argument was out of the range of valid values. Parameter name: 1880729.PDF.»

Vi trengjer ei spørjing for å finne slike, kanskje òg ei liste frå fillageret.

<!--
```sql
SQL> select * from dokversjon where ve_dokid_db = 1518112;
REM INSERTING into DOKVERSJON
SET DEFINE OFF;
Insert into DOKVERSJON (VE_DOKID_DB,VE_VERSJON,VE_VARIANT_VF,VE_AKTIV,VE_DOKFORMAT_LF,VE_REGAV_PN,VE_OPPBEDATO,VE_TGKODE
_TK,VE_ARKMERK,VE_LAGRENH,VE_FILREF,VE_DOKMALID_G,VE_HARFLEREDOK_G,VE_SJEKKETUT_G,VE_LOCFILREF_G,VE_PRIVFILREF_G,VE_TRIE
DCONVERT_G,VE_OPPDDATO_G,VE_OPPDAV_G,VE_DIV0_G,VE_DIV1_G,VE_DIV2_G,VE_DIV3_G,VE_DIV4_G,VE_DIV5_G,VE_DIV6_G,VE_DIV7_G,VE_
DIV8_G,VE_DIV9_G,VE_RESERVERTAV_G,VE_CONVERTFLAG_G,VE_OPPRETTETDATO_G,VE_SISTOPPDATERT_G,VE_CHECKSUM,VE_FILESIZE,VE_CHEC
KSUM_ALGORITHM) values ('1518112','1','P','-1','RA-PDF','2127',null,null,null,'PROD','2020\11\24\1880729.PDF',null,'-1',
'1052',null,'\\skuld\home\ajfkt\ephorte','0',to_timestamp('24.11.2020','DD.MM.RRRR HH24.MI.SSXFF'),'2127',null,null,null
,null,null,null,null,null,null,null,null,null,to_timestamp('24.11.2020','DD.MM.RRRR HH24.MI.SSXFF'),to_timestamp('20.04.
2021','DD.MM.RRRR HH24.MI.SSXFF'),'7C0D6495B57769433488B731F61DFAF9D70A39CDFCC44E491C173489C952D6D5','45210','SHA256');

1 row selected.
```
-->


## Byt ut dokument som PdfConverter ikkje kan konvertere [SQL, prosjekt]

Bruk arbeidsarket «Dok-til-manuell-konvertering» for å finne dokumenta det gjeld.

Ta for deg éit filformat om gongen. Ver varsam og ta vare på fila med det gamle formatet til du har kontrollert at ingen informasjon av verdi har gått tapt i den nye. Den gamle fila blir nemleg sletta frå dokumentlageret når du lastar opp den nye.

Nedanfor er det skissert ein mogeleg framgangsmåte. Det vesentlege er sluttresultatet: at fila har eit arkivformat og er leseleg.


### Vurderingar for kvart filformat {- .unlisted}

Undersøk følgjande for kvart filformat. Dokumenter det du kjem fram til.

* Er filformatet alt eit godkjent filformat for arkivdokument ved avlevering eller deponering, jf. [Riksarkivaren si forskrift §§\ 5-17 til 5-19](https://lovdata.no/forskrift/2017-12-19-2286/§5-17)? Då er det ikkje naudsynt å gjere noko meir.
* Klarer ephorte å konvertere fila til eit arkivformat utan at naudsynt informasjon går tapt, dersom du vel «Konverter til PDF»? (I arbeidsarket kan du sjå om det finst eit arkivformat av fila.) Då er det ikkje naudsynt å gjere noko meir.

> **NB:** Du må gjere produksjonsformatet til den aktive versjonen før du gjer dette, og før det eventuelt sette journalposten i status M og endre saka sin status frå A (og deretter tilbake). Følg rettleiinga nedanfor.

* Inneheld filene i filformatet berre rein tekst. Då er det ikkje naudsynt å gjere noko meir.

Dersom ikkje prøv å finne ut korleis du kan konvertere fila. 

Det kan hende du må laste ned spesialprogramvare. Men ikkje last ned slikt rask som gjev virus og slikt. Er du usikker, spør ein ekspert.


### Slik konverterer du ei fil manuelt {- .unlisted}

1. Dersom saka har status A, set ho i status R. 
1. Set journalposten i status M. 
1. Dersom produksjonsformatet ikkje er det aktive formatet, gjer det til det. 
1. Sjekk ut dokumentet. Merk deg filnamnet.
1. Opn Filutforskar, skriv `%temp%` i adresselinja og trykk enter. Då kjem du til mappa der fila er. Sorter filene etter datoen då dei sist vart endra, slik at den som vart sist endra, er øvst.
1. *Dersom det er ikkje er ei zipfil eller liknande (rar, tar.gz …):*
    i. Opn fila. Konverter ho til eit godkjent arkivformat eller til eit format som ephorte sjølv kan konvertere.

        Dersom det er eit rekneark, endre òg utskriftsformatet slik at alle kolonnene kjem med på éi side: I Excel vel Skriv ut, liggjande utskriftsformat, «Fit All Columns on One Page» og lagr fila (ikkje skriv ho ut).

    i. Lat att eventuelle program fila er open i. 
    i. Opn menyen til dokumentet i ephorte og vel «Nytt tekstdokument». 
    i. Vel «Hent fil fra disk». 
    i. Skriv `%temp%` i adresselinja og trykk enter. Då kjem du til mappa der fila er. Vel ho og last ho opp.
    i. Stadfest at du vil skrive over fila i ephorte. No vert den opprinnelige fila sletta. Derfor er det viktig at du ikkje sletter fila du lasta ned, før du er heilt sikker på at innhaldet er bevart i fila du har lasta opp.
    i. Konverter fila til PDF i ephorte. 
    i. Sjekk fila (arkivformatet som ephorte har laga) ser fin ut. 

        **NB:** Når du gjer dette, vert den gamle fila sletta! Derfor er det viktig å ikkje slette den fila som du lasta ned, før du er sikker på at den nye fila i ephorte er leseleg. 

    *Dersom det er ei zip-fil (eller liknande):*

    i. Pakk ho ut.

        Her må du gjere [vurderingane over][Vurderingar for kvart filformat] for kvar einskild fil ho inneheld. Dersom ho berre inneheld filer du ikkje treng å konvertere, treng du berre å laste dei opp som skildra nedanfor. Elles må du konvertere dei som skildra i denne rettleiinga.

    i. Lat att eventuelle program filene er open i. 
    i. Kopier stien til mappa med filene.
    i. Vel «Importer filer» (i menyen for fanen «Dokumenter» i ephorte).
    i. Gå til mappa med filene som var i zip-fila, marker alle filene og last dei opp. Endre namn dersom du trur at nokon ein gong vil ha interesse av disse filene.
    i. Kontroller at fila/filene ser fin/fine ut. 

        Dersom det ein del filer i zip-fila, kan du opne menyen for journalposten, velje Funksjoner og «Vis sammenstilling av dok.» for å kontrollere dei .

    i. Slett vedlegget med zip-fila. No vert den opprinnelige fila sletta. Derfor er det viktig at du ikkje sletter fila du lasta ned, før du er heilt sikker på at innhaldet er bevart i filene du har lasta opp.

1. Set journalposten tilbake i den statusen han hadde. 
1. Set saka tilbake i den statusen ho hadde. 

### Kjende filformat

Filformat | Merknad
:--|:----
DOCX, WORD, DOT, DOTX, DOCM, RTF, ODT | Tekstdokument; vert konvertert av PdfConverter.
XLS, XLSX, ODS | Rekneark; vert konvertert av PdfConverter.
POWERPOINT, PPTX | Presentasjon; vert konvertert av PdfConverter.
HTM | Vert konvertert av PdfConverter.
MSG | Vert konvertert av PdfConverter.
BMP, PNG, TIFF, JPG, JPEG, GIF | Biletfiler; vert konvertert av PdfConverter.
XML | Er eit godkjent filformat for arkivdokument.
VCF, CSV, BIB, TEX, ASC | Er (som regel) rein tekst.
DAT | Som PDF; men må konverterast manuelt.
EML | =\ E-MaiL; må konverterast manuelt.
DOC0 | =\ DOC
MDI | Kan konverterast med [MDI to TIFF File Converter](https://www.microsoft.com/en-ca/download/details.aspx?id=30328).
XLSB | Kan konverterast med Excel.
MHT | Kan behandlast og konverterast som HTML av PdfConverter?
ZIP, RAR | Inneheld ei eller fleire filer; må pakkast opp.


## Send alle dokumenta til konvertering [SQL]

Gjer dette etter at du har bytt ut dokumenta som PdfConverter ikke kan konvertere.


## Kontroller dokumenta [teknisk, prosjekt]

Last dei ned frå dokumentlageret.

Ta stikkprøver av arkivformat av dokument: Lag ei liste og sjekk disse.

Bruk veraPDF til å validere alle arkivformat med PDF-dokument.

Identifiser alle PDF Portfolio-filer (til dømes med `pdfdetach`) og konverter dei manuelt.

Dokumenter og kommenter fordeling av filformat. Nemn kva for filformat som har rein tekst.

# Tett hol

1. Kartlegg prosessane i verksemda (lett).
1. Avgjer kva for prosessar som skal bevarast (eller har høg kassasjonsfrist).
1. Kartlegg kva som er i arkivet
1. Prøv å få tak i alt som manglar, som er bevaringsverdig.

# Ymse

```sql
-- Vel talet på restansar der mottakaren vart oppdatert for over eit år sidan, fordelt på JP-status
select null as dummy, count(*) antall, jp_status_js as status from avsmot
inner join journpost on am_jpid = jp_id
inner join noarksak on sa_id = jp_said
inner join arkivdel on sa_arkdel_ad = ad_arkdel
where 1=1
    and ad_arkiv_ar in &s_ar
    and jp_status_js != 'U'
    and am_harrestanse_g = -1
    and am_sistoppdatert_g < (sysdate - 365)
group by jp_status_js
order by antall desc
;
```

# Produser uttrekk

# Nyttige lenkjer

Arkivverket si nettside om periodisering: <https://www.arkivverket.no/for-arkiveiere/periodisering>
