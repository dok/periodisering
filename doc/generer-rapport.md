---
title: "Korleis generere rapporten med SQLcl og konvertere han med Pandoc og eit bash-skript"
...

Skripta er testa med testa med SQLcl. 

1. Køyr `make rapportar` for å sette inn vald(e) arkiv i fila i `utdata/rapportar/`.

1. Kople til databasen med SQLcl.

1. Gjer slik at SQLcl ser etter skript i mappa kor dei er (så slepp du å skrive inn heile stien):

    ```sql
    SQL> cd c:\temp\Periodisering
    ```

    (Du kan sjekke om SQLcl alt ser etter skript i mappa med kommandoen `show sqlpath`.)

1. Køyr skriptet. Med `set termout off` vert resultatet ikkje vist i kommandolinja.

    ```sql
    SQL> set termout off
    SQL> @tal-for-valde-arkiv.sql
    ```

1. Formater rapporten. Ved hjelp av iconv, sed og Pandoc får du ei HTML-fil med UTF-8-teiknkoding og med tal som har harde mellomrom. 

    ```bash
    $ bash skript/formater-rapport.sh FILNAMN.md
    ```


# Om teiknkoding (encoding)

*Det same gjeld etter at vi tok i bruk ein database med UTF-8-teiknkoding.*

Databasen brukar ISO-8859-1-teiknkoding:

```sql
SQL> select parameter, value from v$nls_parameters where parameter = 'NLS_CHARACTERSET';

PARAMETER        VALUE
---------------- ------------
NLS_CHARACTERSET WE8ISO8859P1
```

Det same brukar Windows-maskina som Oracle SQL Developer, SQL\*Plus- og SQLcl-klientane køyrer på:

```ps
PS > [System.Text.Encoding]::Default

IsSingleByte      : True
BodyName          : iso-8859-1
EncodingName      : Western European (Windows)
...
```

Når me køyrer SQL\*Plus skript med UTF-8-teiknkoding, har tabellane ISO-8859-1-teiknkoding, mens det me skriv med `prompt`, har UTF-8-teiknkoding. Ved å konvertere SQL\*Plus-skriptet til ISO-8859-1-teiknkoding får fila med resultatet same teiknkoding. Den kan me deretter konvertere til UTF-8 med `iconv -f ISO-8859-1 -t UTF-8`.
