Dette arkivet inneheld:

* eit (uferdig) framlegg til framgangsmåte for periodisering av arkiv og høgttenking om periodisering med skript (`doc/framlegg-til-framgangsmåte.md`)
* fleire arbeidsark for å utføre oppgåver i forbindelse med periodisering av arkiv
* skript for å lage ein rapport med tal for éin eller fleire arkiv

Kjeldefilene til arbeidsarka ligg i mappa `src`.

# Kom i gang

1. Rediger `Makefile` og angi i line\ 4 arkivet eller arkiva du vil inkludere i arbeidsarka og i rapporten.

1. Køyr `make` i Linux kommandoline.

Då vert det laga SQL-filer i mappa `utdata` som du kan bruke til å hente ut informasjon frå databasen. Sjå `doc/arbeidsark.md` og `doc/generer-rapport.md` for informasjon om det.

Køyr `make doc` for å konvertere Markdown-filene (i `doc`-mappa) til HTML-filer (i `utdata`-mappa).

Sjå `doc/framlegg-til-framgangsmåte.md` for høgttenking om periodisering med skript.


# Framtidsplanar

* SQL-uttrykk for massekonvertering
* PDF: `skript/pdftests.sh` gjev informasjon om PDF-filer.

… med meir


# Føresetnadar

* Linux kommandoline (til dømes [WSL](https://docs.microsoft.com/en-us/windows/wsl/))
* [Pandoc](https://pandoc.org/)

Dei fleste skripta er testa med ephorte/Elements 2021.01 (frå Sikri) og Oracle Database 19c. Nokre har berre vert testa med ephorte 5.5.3 og/eller Oracle Database 18c.
