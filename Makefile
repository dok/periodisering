SHELL := /bin/bash
# Køyr `make ARKIV="('ARKIV1', 'ARKIV2')"` for å velje andre arkiv.
# arkiv formatert som innhaldet i eit in-vilkår (in condition) med apostrofar rundt kvart namn
# https://docs.oracle.com/en/database/oracle/oracle-database/19/sqlrf/IN-Condition.html#GUID-C7961CB3-8F60-47E0-96EB-BDCF5DB1317C
arkiv = "('ARKIV')"

# Lag ei liste med alle utfilene ved hjelp av alle innfilene som finst:
alle_arbeidsark = $(shell ls src/arbeidsark/*.src | sed -e 's/^src/utdata/' -e 's/\.src$$/\.sql/')
alle_rapportar = $(shell ls src/rapportar/*.sql | sed -e 's/^src/utdata/')
all_dokumentasjon = $(shell ls doc/*.md | sed -e 's/^/utdata\//' -e 's/\.md$$/\.html/')

arbeidsark_mal = build/arbeidsark-mal.sh

.PHONY: utdata/arbeidsark utdata/rapport clean doc

all: utdata/arbeidsark $(alle_arbeidsark) utdata/arbeidsark/alle.sql utdata/rapportar $(alle_rapportar) utdata/doc $(all_dokumentasjon)

# Lag mapper
utdata/arbeidsark utdata/rapportar utdata/doc:
	mkdir -p $@

# Køyr bash-skriptet src/arbeidsark/mal.sh med argumenta
# 1) innfil (prerequisite) ($<)
# 2) namn på arkiv ($(arkiv))
# 3) namn på utfil ($(basename $(notdir $@))) (jf. https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html)
# Og skriv resultatet til målet (target) ($@)
utdata/arbeidsark/%.sql: src/arbeidsark/%.src $(arbeidsark_mal)
	bash $(arbeidsark_mal) $< $(arkiv) "$(basename $(notdir $@))" > $@

# Lag fila alle.sql med alle sql-filene utanom alle.sql i målmappa:
utdata/arbeidsark/alle.sql: FORCE
	ls -I "$(notdir $@)" "$(dir $@)" | sed -e 's/^/@/' -e 's/$$/ \&1/' > $@


rapportar: $(alle_rapportar)

# Lag rapportar
utdata/rapportar/%.sql: src/rapportar/%.sql
	echo "$<"
	cp $< $@
	sed -i -e "s/&1/\$(arkiv)\""/" $@


# Konverter Markdown-filer i doc/ til HTML

doc_filer = $(addprefix utdata/,$(addsuffix .html, $(basename $(wildcard doc/*.md))))

doc: $(doc_filer)

utdata/doc/%.html: doc/%.md
	pandoc "$<" --standalone --toc --number-sections --output "$@" \
		--metadata="toc-title=Innhald" \
		--metadata="linkcolor=blue"

FORCE:

# Bindestrek framfor `rm` tyder at seinare kommandoar skal køyrast sjølv om `rm` feiler.
clean:
	-rm -f utdata/arbeidsark/*
